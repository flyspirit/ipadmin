namespace IPAdmin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Patents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IntSearchCode = c.String(maxLength: 256),
                        Name = c.String(maxLength: 256),
                        Owner = c.String(maxLength: 256),
                        Agency = c.String(maxLength: 256),
                        IsDeleted = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        CreateBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SerialNoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SerialNumber = c.String(nullable: false, maxLength: 200),
                        CreateDate = c.DateTime(nullable: false),
                        Patent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patents", t => t.Patent_Id)
                .Index(t => t.Patent_Id);
            
            CreateTable(
                "dbo.SerialNoCustomers",
                c => new
                    {
                        SerialNoId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        ContractNo = c.String(maxLength: 256),
                        InvoiceNo = c.String(maxLength: 256),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MachineId = c.String(maxLength: 256),
                        Order = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.SerialNoId, t.CustomerId })
                .ForeignKey("dbo.SerialNoes", t => t.SerialNoId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.SerialNoId)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(maxLength: 256),
                        Email = c.String(maxLength: 256),
                        Company = c.String(maxLength: 256),
                        Address = c.String(maxLength: 256),
                        CreateDate = c.DateTime(nullable: false),
                        CreateBy = c.String(maxLength: 256),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 256),
                        IsDeleted = c.Boolean(nullable: false),
                        ParentUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfile", t => t.ParentUser_Id)
                .Index(t => t.ParentUser_Id);
            
            CreateTable(
                "dbo.PatentCountries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SearchCode = c.String(maxLength: 50),
                        PatentNumber = c.String(maxLength: 50),
                        ResourceCode = c.String(maxLength: 50),
                        Patent_Id = c.Int(),
                        Country_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patents", t => t.Patent_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Patent_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.PatentCountries", new[] { "Country_Id" });
            DropIndex("dbo.PatentCountries", new[] { "Patent_Id" });
            DropIndex("dbo.UserProfile", new[] { "ParentUser_Id" });
            DropIndex("dbo.SerialNoCustomers", new[] { "CustomerId" });
            DropIndex("dbo.SerialNoCustomers", new[] { "SerialNoId" });
            DropIndex("dbo.SerialNoes", new[] { "Patent_Id" });
            DropForeignKey("dbo.PatentCountries", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.PatentCountries", "Patent_Id", "dbo.Patents");
            DropForeignKey("dbo.UserProfile", "ParentUser_Id", "dbo.UserProfile");
            DropForeignKey("dbo.SerialNoCustomers", "CustomerId", "dbo.UserProfile");
            DropForeignKey("dbo.SerialNoCustomers", "SerialNoId", "dbo.SerialNoes");
            DropForeignKey("dbo.SerialNoes", "Patent_Id", "dbo.Patents");
            DropTable("dbo.Countries");
            DropTable("dbo.PatentCountries");
            DropTable("dbo.UserProfile");
            DropTable("dbo.SerialNoCustomers");
            DropTable("dbo.SerialNoes");
            DropTable("dbo.Patents");
        }
    }
}

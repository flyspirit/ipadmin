﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using IPAdmin.Models;
using IPAdmin.Repository;
using Microsoft.Ajax.Utilities;

namespace IPAdmin.Common
{
    public static class Helper
    {
        public static string[] RoleOrder = new string[] {"Admin", "D1", "D2", "M1", "M2", "EndUser"};

        public static string GetCurrentUserName()
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.User != null)
                {
                    return HttpContext.Current.User.Identity.Name;
                }
            }

            return null;
        }

        public static UserProfile GetCurrentUserDetail()
        {
            if ( GetCurrentUserName() != null )
            {
            }

            return null;
        }

        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly());
            builder.RegisterType<PatentContext>().InstancePerHttpRequest();
            builder.RegisterGeneric(typeof (GenericRepository<>)).InstancePerHttpRequest();
            builder.RegisterType<UnitOfWork>().PropertiesAutowired().InstancePerHttpRequest();

            return builder.Build();
        }
        
        public static string[] GetExcludedRoles(string[] roles)
        {
            int max = 0;
            foreach (var role in roles)
            {
                int index = Array.IndexOf(RoleOrder, role);
                if (index > max)
                    max = index;
            }

            return RoleOrder.Where((x, i) => i <= max).ToArray();
        }
    }
}
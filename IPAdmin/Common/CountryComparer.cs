﻿using System;
using System.Collections.Generic;
using IPAdmin.Models;

namespace IPAdmin.Common
{
    public class CountryComparer : IEqualityComparer<Country>
    {
        public bool Equals(Country x, Country y)
        {
            //Check whether the compared objects reference the same data. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null. 
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the key is equal. 
            return x.Id == y.Id;
        }

        public int GetHashCode(Country obj)
        {
            //Check whether the object is null 
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Calculate the hash code for the object. 
            return obj.Id;
        }
    }
}
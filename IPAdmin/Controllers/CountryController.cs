﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IPAdmin.Filters;
using IPAdmin.Models;
using IPAdmin.Repository;

namespace IPAdmin.Controllers
{
    [InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class CountryController : Controller
    {
        private UnitOfWork _unitOfWork;

        public CountryController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //
        // GET: /Country/

        public ActionResult Index()
        {
            return View(_unitOfWork.CountryRepository.Get());
        }

        //
        // GET: /Country/Details/5

        public ActionResult Details(int id = 0)
        {
            if (id == 0)
                return HttpNotFound();

            Country country = _unitOfWork.CountryRepository.GetByID(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        //
        // GET: /Country/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Country/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Country country)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.CountryRepository.Insert(country);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }

            return View(country);
        }

        //
        // GET: /Country/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Country country = _unitOfWork.CountryRepository.GetByID(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        //
        // POST: /Country/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Country country)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.CountryRepository.Update(country);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(country);
        }

        //
        // GET: /Country/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Country country = _unitOfWork.CountryRepository.GetByID(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        //
        // POST: /Country/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Country country = _unitOfWork.CountryRepository.GetByID(id);
            _unitOfWork.CountryRepository.Delete(country);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
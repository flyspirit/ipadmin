﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using IPAdmin.Common;
using IPAdmin.Models;
using IPAdmin.Repository;
using IPAdmin.ViewModels;
using WebMatrix.WebData;

namespace IPAdmin.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize]
    public class PatentController : Controller
    {
        private UnitOfWork _unitOfWork;

        public PatentController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //
        // GET: /Patent/
        public ActionResult Index()
        {
            IEnumerable<Patent> patents;

            // only admin user can see all patents
            // non admin user can only see those patents assgiend to them
            if (!Roles.IsUserInRole("Admin"))
            {
                patents = _unitOfWork.SerialNoCustomerRepository.Get(
                    s => s.Customer.UserName == User.Identity.Name && !s.Customer.IsDeleted && !s.SerialNo.Patent.IsDeleted,
                    null, "SerialNo, SerialNo.Patent").Select( s=>s.SerialNo.Patent).Distinct().ToList();
            }
            else
            {
                patents = _unitOfWork.PatentRepository.Get(p => p.IsDeleted == false).ToList();
            }
            return View(patents);
        }

        //
        // GET: /Patent/Details/5
        public ActionResult Details(int id = 0)
        {
            var patent = _unitOfWork.PatentRepository.Get(p => p.IsDeleted == false && p.Id == id, null,
                "SerialNoes.SerialNoCustomers.Customer, PatentCountries.Country").FirstOrDefault();

            if (patent == null)
            {
                return HttpNotFound();
            }
            return View(patent);
        }

        [HttpPost]
        public ActionResult Details(IEnumerable<SerialNoSelection> selections)
        {
            int x = 0;

            return View();
        }
        //
        // GET: /Patent/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            PatentViewModel patent = new PatentViewModel {AvailablePatentCountries = new Collection<PatentCountry>()};
            var countries = _unitOfWork.CountryRepository.Get().ToList();

            foreach (var country in countries)
            {
                patent.AvailablePatentCountries.Add(new PatentCountry{ Country = country });
            }

            return View(patent);
        }

        //
        // POST: /Patent/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(PatentViewModel patentViewModel)
        {
            if (ModelState.IsValid)
            {
                Patent patent = new Patent
                {
                    IntSearchCode = patentViewModel.IntSearchCode,
                    Owner = patentViewModel.Owner,
                    Name = patentViewModel.Name,
                    Agency = patentViewModel.Agency
                };

                foreach (var viewModel in patentViewModel.AvailablePatentCountries)
                {
                    if (!string.IsNullOrWhiteSpace(viewModel.SearchCode))
                    {
                        var country = _unitOfWork.CountryRepository.GetByID(viewModel.Country.Id);

                        patent.PatentCountries.Add(new PatentCountry{ Country = country, Patent = patent, SearchCode = viewModel.SearchCode, ResourceCode = viewModel.ResourceCode ?? string.Empty});
                    }

                }
                patent.CreateDate = DateTime.Now;
                patent.CreateBy = WebSecurity.CurrentUserName;

                _unitOfWork.PatentRepository.Insert(patent);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(patentViewModel);
        }

        //
        // GET: /Patent/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            var patent =_unitOfWork.PatentRepository.Get(p => p.Id == id && p.IsDeleted == false, null,
                    "SerialNoes.SerialNoCustomers.Customer, PatentCountries.Country").FirstOrDefault();

            if (patent == null)
            {
                return HttpNotFound();
            }

            PatentViewModel patentViewModel = new PatentViewModel(patent);
            // countries this patent already published to
            if (patent.PatentCountries != null)
                patentViewModel.AvailablePatentCountries = patent.PatentCountries.ToList();

            // merge countries
            var countries = _unitOfWork.CountryRepository.Get().ToList();
            countries = countries.Except(patentViewModel.AvailablePatentCountries.Select(pc => pc.Country), new CountryComparer()).ToList();
            foreach (var country in countries)
            {
                patentViewModel.AvailablePatentCountries.Add(new PatentCountry { Country = country });
            }

            return View(patentViewModel);
        }

        //
        // POST: /Patent/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(PatentViewModel patentViewModel)
        {
            if (ModelState.IsValid)
            {
                var patent = _unitOfWork.PatentRepository.Get(p => p.Id == patentViewModel.Id && p.IsDeleted == false, null,
                        "SerialNoes.SerialNoCustomers.Customer, PatentCountries.Country").FirstOrDefault();

                patent.IntSearchCode = patentViewModel.IntSearchCode;
                patent.Owner = patentViewModel.Owner;
                patent.Name = patentViewModel.Name;
                patent.Agency = patentViewModel.Agency;

                foreach (var patentCountry in patentViewModel.AvailablePatentCountries)
                {
                    var existing = patent.PatentCountries.FirstOrDefault(pc => pc.Country.Id == patentCountry.Country.Id);

                    if (existing != null)
                    {
                        existing.SearchCode = patentCountry.SearchCode;
                        existing.ResourceCode = patentCountry.ResourceCode;
                        existing.PatentNumber = patentCountry.PatentNumber;
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(patentCountry.SearchCode))
                        {
                            var country = _unitOfWork.CountryRepository.GetByID(patentCountry.Country.Id);
                            patent.PatentCountries.Add(new PatentCountry
                            {
                                Country = country,
                                Patent = patent,
                                SearchCode = patentCountry.SearchCode,
                                PatentNumber = patentCountry.PatentNumber ?? string.Empty,
                                ResourceCode = patentCountry.ResourceCode ?? string.Empty
                            });
                        }
                    }
                }

                _unitOfWork.PatentRepository.Update(patent);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /Patent/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            Patent patent = _unitOfWork.PatentRepository.GetByID(id);
            if (patent == null || patent.IsDeleted)
            {
                return HttpNotFound();
            }
            return View(patent);
        }

        //
        // POST: /Patent/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Patent patent = _unitOfWork.PatentRepository.GetByID(id);
            if (patent == null || patent.IsDeleted)
            {
                return HttpNotFound();
            }

            patent.IsDeleted = true;
            _unitOfWork.PatentRepository.Update(patent);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        [ActionName("Generate")]
        [Authorize(Roles = "Admin")]
        public ActionResult GenerateSerialNumber(int id)
        {
            Patent patent = _unitOfWork.PatentRepository.Get(p => p.Id == id, null, "PatentCountries, PatentCountries.Country").FirstOrDefault();

            if (patent == null || patent.IsDeleted)
                return HttpNotFound();

            if (!patent.PatentCountries.Any())
                throw new ApplicationException("This patent doesn't exist in any country");

            return View(new GenerateSerialNo { Patent = patent, Count = 1});
        }


        [HttpPost]
        [ActionName("Generate")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult GenerateSerialNumber(GenerateSerialNo generateSerialNo)
        {
            var patent = _unitOfWork.PatentRepository.GetByID(generateSerialNo.Patent.Id);
            if (patent == null || patent.IsDeleted)
                return HttpNotFound();

            _unitOfWork.PatentRepository.LoadCollection(patent, "SerialNoes");

            if (patent.SerialNoes == null)
                patent.SerialNoes = new List<SerialNo>();

            // retreive user object
            var customer = _unitOfWork.UserProfileRepository.Get(c => c.UserName == User.Identity.Name).FirstOrDefault();

            var sns = (from s in patent.SerialNoes
                where s.SerialNumber.StartsWith(generateSerialNo.Prefix) && s.CreateDate > DateTime.Today
                orderby s.Id
                select s).LastOrDefault();

            int startNumber;
            if (sns == null)
            {
                startNumber = int.Parse(DateTime.Now.ToString("yyMMdd") + "0001");
            }
            else
            {
                int maxNumber = int.Parse(sns.SerialNumber.Split('-').Last());
                startNumber = maxNumber + 1;
            }

            for (int i = 0; i < generateSerialNo.Count; i++)
            {
                var sn = new SerialNo
                {
                    Patent = patent,
                    SerialNumber = generateSerialNo.Prefix + "-" + (startNumber+i).ToString(),
                    CreateDate = DateTime.Now
                };
                SerialNoCustomer snc = new SerialNoCustomer
                {
                    Customer = customer,
                    CustomerId = customer.Id,
                    SerialNo = sn,
                    SerialNoId = sn.Id,
                    InvoiceNo = " ",      // empty invoiceNo for admin
                    Currency = "AUD",
                    Price = 0.0m,
                    CreateDate = DateTime.Now
                };

                sn.SerialNoCustomers = new Collection<SerialNoCustomer>();
                sn.SerialNoCustomers.Add(snc);
                patent.SerialNoes.Add(sn);
            }

            _unitOfWork.PatentRepository.Update(patent);
            _unitOfWork.Save();

            return RedirectToAction("Details", patent);
        }
    }
}
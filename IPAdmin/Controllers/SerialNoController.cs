﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using IPAdmin.Common;
using IPAdmin.Filters;
using IPAdmin.Models;
using IPAdmin.Repository;
using IPAdmin.ViewModels;
using Microsoft.Ajax.Utilities;
using WebGrease.Css.Extensions;
using WebMatrix.WebData;

namespace IPAdmin.Controllers
{
    [InitializeSimpleMembership]
    [Authorize]
    public class SerialNoController : Controller
    {
        private UnitOfWork _unitOfWork;
        private GenericRepository<SerialNo> _serialNoRepository;
        private GenericRepository<SerialNoCustomer> _serialNoCustomerRepository;
        private UserProfileRepository _userProfileRepository;
        //
        // GET: /Customer/

        public SerialNoController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _serialNoRepository = _unitOfWork.SerialNoRepository;
            _serialNoCustomerRepository = _unitOfWork.SerialNoCustomerRepository;
            _userProfileRepository = _unitOfWork.UserProfileRepository;
        }

        //
        // GET: /SerialNo/id

        public ActionResult Index(int id)
        {
            int userId = WebSecurity.CurrentUserId;

            var snNoCustomers = _serialNoCustomerRepository.Get(s => s.SerialNoId == id, q => q.OrderBy(s => s.Order),
                "SerialNo.Patent, Customer").ToList();

            if (!snNoCustomers.Any())
                return HttpNotFound();

            if (snNoCustomers.Any(sc => sc.CustomerId == userId) == false)
                throw new ApplicationException("Current user is not part of this serial number");

            return View(snNoCustomers);
        }

        public ActionResult Create(int id)
        {
            var serialNo = _serialNoRepository.Get(s => s.Id == id, null, "SerialNoCustomers.Customer").FirstOrDefault();
                if (serialNo == null)
                    return HttpNotFound();

            var allocatedCustomers =_serialNoCustomerRepository.Get(sc => sc.SerialNoId == id && !sc.Customer.IsDeleted).Select(sc => sc.CustomerId).ToArray();
            var candidateCustomers = GetAvailableCustomers(new List<SerialNo>(){serialNo}).Where(c => !allocatedCustomers.Contains(c.UserId)).ToList();

            var ac = new AvailableCustomer {SerialNo = serialNo, Customers = candidateCustomers, SerialNoId = id, Currency = "AUD"};
            return View(ac);
        }

        private IEnumerable<UserRole> GetAvailableCustomers(IEnumerable<SerialNo> serialNoes)
        {
            IList<UserRole> userRoles = new List<UserRole>();

            var currentUser = _userProfileRepository.Get(u => u.Id == WebSecurity.CurrentUserId, null, "Customers").SingleOrDefault();
            if (currentUser == null)
                throw new ApplicationException("Inavlid login user");

            IList<string> lastOrderCustomerRoles = new List<string>();
            foreach (var serialNo in serialNoes)
            {
                var lastOrderUser = serialNo.SerialNoCustomers.Where(sc=>!sc.Customer.IsDeleted).OrderByDescending(s => s.Order).First();
                lastOrderCustomerRoles.Add(Roles.GetRolesForUser(lastOrderUser.Customer.UserName).First());
            }
            
            //lastOrderCustomerRoles.Add(Roles.GetRolesForUser().First());
            //string role = Helper.GetLowestRole(lastOrderCustomerRoles.ToArray());

            // get current user roll
            string role = Roles.GetRolesForUser().FirstOrDefault();
            lastOrderCustomerRoles.Add(role);
            var excludedRoles = Helper.GetExcludedRoles(lastOrderCustomerRoles.Distinct().ToArray()); 

            if (!string.IsNullOrWhiteSpace(role))
            {
                if (role == "Admin")
                {
                    var users = _userProfileRepository.GetAllActive().ToList();

                    if (users != null && users.Any())
                    {
                        userRoles = users.Where(u => !excludedRoles.Contains(Roles.GetRolesForUser(u.UserName).First()))
                                .Select(u => new UserRole(u))
                                .ToList();
                    }
                }
                else if (role == "D1")
                {
                    // can deliver to D2 only
                    userRoles = currentUser.Customers.Where(u => !u.IsDeleted && Roles.IsUserInRole(u.UserName, "D2")).Select(u => new UserRole(u)).ToList();
                }
                else if (role == "D2")
                {
                    // can deliver to M1 only
                    //userRoles = db.UserProfiles.Where(u => Roles.IsUserInRole("M1")).Select(u => new UserRole(u)).ToList();
                    userRoles = currentUser.Customers.Where(u => !u.IsDeleted && Roles.IsUserInRole(u.UserName, "M1")).Select(u => new UserRole(u)).ToList();
                }
                else if (role == "M1")
                {
                    // can deliver to M2 or end user
                    userRoles = currentUser.Customers.Where(u => !u.IsDeleted && Roles.IsUserInRole(u.UserName, "M2") || Roles.IsUserInRole(u.UserName, "EndUser")).Select(u => new UserRole(u)).ToList();
                }
                else if (role == "M2")
                {
                    // can deliver to end user only 
                    userRoles = currentUser.Customers.Where(u => !u.IsDeleted && Roles.IsUserInRole(u.UserName, "EndUser")).Select(u => new UserRole(u)).ToList();
                }
            }
            
            return userRoles;
        }


        [HttpPost]
        public ActionResult Create(AvailableCustomer availableCustomer)
        {
            var sn = _serialNoRepository.GetByID(availableCustomer.SerialNoId);
            if (sn == null)
                return HttpNotFound();

            var customer = _userProfileRepository.GetByID(availableCustomer.SelectedCustomerId);

            int maxOrder =
                _serialNoCustomerRepository.Get(snc => snc.SerialNoId == sn.Id, q => q.OrderBy(snc => snc.Order))
                    .Select(snc => snc.Order)
                    .Max();

            SerialNoCustomer nsc = new SerialNoCustomer
            {
                Customer = customer,
                CustomerId = customer.Id,
                Order = ++maxOrder,
                SerialNo = sn,
                SerialNoId = sn.Id,
                ContractNo = availableCustomer.ContractNo,
                InvoiceNo = availableCustomer.InvoiceNo,
                Currency = availableCustomer.Currency,
                Price = availableCustomer.Price,
                MachineId = (string.IsNullOrWhiteSpace(availableCustomer.MachineId) ? null : availableCustomer.MachineId), 
                CreateDate = DateTime.Now
            };

            _serialNoCustomerRepository.Insert(nsc);
            _unitOfWork.Save();

            return RedirectToAction("Index", new { id = sn.Id });
        }

        [HttpPost]
        public ActionResult AssignCustomer(IEnumerable<SerialNoSelection> selections, string path)
        {
            var serialIds = selections.Where(s => s.Selected).Select(s => s.SerialNo.Id).ToList();
            if (!serialIds.Any())
            {
                TempData["Message"] = "You must select serial numbers first before assign to customer";
                return Redirect(path);
            }

            var serialNos = _serialNoRepository.Get(s => serialIds.Contains(s.Id), null, "Patent, SerialNoCustomers.Customer").ToList();

            var allocatedCustomers = _serialNoCustomerRepository.Get(sc => serialIds.Contains(sc.SerialNoId) && !sc.Customer.IsDeleted).Select(sc => sc.CustomerId).ToArray();
            var candidateCustomers = GetAvailableCustomers(serialNos).Where(c => !allocatedCustomers.Contains(c.UserId)).ToList();

            var ac = new AssignSerialNos() { SerialNo = serialNos, Customers = candidateCustomers, Currency = "AUD"};

            return View(ac);
        }

        [HttpPost]
        public ActionResult DoBatchAssign(AssignSerialNos assignSerialNos)
        {
            foreach (var id in assignSerialNos.SerialNo.Select(s=>s.Id))
            {
                var sn = _serialNoRepository.GetByID(id);
                if (sn == null)
                    throw new ArgumentException(string.Format("Can not find record for Id: {0}", id));

                var customer = _userProfileRepository.GetByID(assignSerialNos.SelectedCustomerId);

                int maxOrder =
                    _serialNoCustomerRepository.Get(snc => snc.SerialNoId == sn.Id, q => q.OrderBy(snc => snc.Order))
                        .Select(snc => snc.Order)
                        .Max();

                SerialNoCustomer nsc = new SerialNoCustomer
                {
                    Customer = customer,
                    CustomerId = customer.Id,
                    Order = ++maxOrder,
                    SerialNo = sn,
                    SerialNoId = sn.Id,
                    ContractNo = assignSerialNos.ContractNo,
                    InvoiceNo = assignSerialNos.InvoiceNo,
                    Currency = String.IsNullOrEmpty(assignSerialNos.Currency) ? "AUD" : assignSerialNos.Currency,
                    Price = assignSerialNos.Price,
                    MachineId = String.IsNullOrEmpty(assignSerialNos.MachineId) ? null : assignSerialNos.MachineId,
                    CreateDate = DateTime.Now
                };

                _serialNoCustomerRepository.Insert(nsc);
            }

            _unitOfWork.Save();

            return RedirectToAction("Index", "Patent");
        }

        public ActionResult PrintChild(int id)
        {
            var customer =_serialNoCustomerRepository.Get(sc => sc.SerialNoId == id, q => q.OrderBy(sc => sc.Order),
                    "Customer, SerialNo, SerialNo.Patent").LastOrDefault();

            if (customer == null)
                throw new ApplicationException("The serial number is invalid or hasn't been assigned");

            string content;

            using (var reader = new StreamReader(Server.MapPath(@"\Content\CertificateTemplate.html"), Encoding.GetEncoding("GB2312")))
            {
                content = reader.ReadToEnd();
                content = content.Replace("%COMPANY_NAME%", customer.Customer.Company);
                content = content.Replace("%SERIAL_NO%", customer.SerialNo.SerialNumber.ToString());
                content = content.Replace("%MACHINE_ID%", customer.MachineId);
                content = content.Replace("%CERTIFY_NOTE%", String.Empty);
                content = content.Replace("%CERTIFY_DATE%", customer.CreateDate.ToString("yyyy MMMM dd"));
                content = content.Replace("%PATENT_NUMBER%", customer.SerialNo.Patent.IntSearchCode + "_" + customer.SerialNo.SerialNumber + "_" + customer.SerialNoId);
            }

            return Content(content, "text/html", Encoding.GetEncoding("GB2312"));
        }
    }
}

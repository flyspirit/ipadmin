﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using IPAdmin.Common;
using IPAdmin.Filters;
using IPAdmin.Models;
using IPAdmin.Repository;
using WebMatrix.WebData;


namespace IPAdmin.Controllers
{
    [InitializeSimpleMembership]
    [Authorize]
    public class CustomerController : Controller
    {
        //private PatentContext db = new PatentContext();
        private UnitOfWork _unitOfWork;
        private UserProfileRepository _customeRepository;
        //
        // GET: /Customer/

        public CustomerController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _customeRepository = _unitOfWork.UserProfileRepository;
        }

        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            // return all users for admin
            if (Roles.IsUserInRole("Admin"))
                return View(_customeRepository.GetAllActive());

            // return users developed by current user
            var user = _customeRepository.Get( c => c.Id == WebSecurity.CurrentUserId, null, "Customers").FirstOrDefault();
            return View(user.Customers.Where(c => c.IsDeleted == false));
        }

        //
        // GET: /Customer/Details/5

        public ActionResult Details(int id = 0)
        {
            //var customer = (from c in db.UserProfiles.Include(u => u.SerialNoCustomers.Select(sn=>sn.SerialNo)).Include(u=>u.ParentUser)
            //    where c.Id == id
            //    select c).FirstOrDefault();

            //var customer = _customeRepository.Get(c => c.Id == id && !c.IsDeleted, null, "ParentUser, SerialNoCustomers.SerialNo.SerialNoCustomers.Customer").FirstOrDefault();

            var customer = _customeRepository.Get(c => c.Id == id && !c.IsDeleted, null, "ParentUser, SerialNoCustomers.SerialNo.Patent").FirstOrDefault();
            
            if (customer == null)
            {
                return HttpNotFound();
            }

            if (!Roles.IsUserInRole("Admin"))
            {
                // return 404 if current user not the viewed customer's parent or the cus tomer is deleted
                if (customer.ParentUser_Id != WebSecurity.CurrentUserId)
                    return HttpNotFound();
            }
            return View(customer);
        }

        //
        // GET: /Customer/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var customer = _customeRepository.GetByID(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        //
        // POST: /Customer/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserProfile customer)
        {
            var dbCustomer = _customeRepository.GetByID(customer.Id);
            if (ModelState.IsValid)
            {
                dbCustomer.UserName = customer.UserName;
                dbCustomer.Address = customer.Address;
                dbCustomer.Company = customer.Company;
                dbCustomer.Email = customer.Email;
                dbCustomer.UpdateDate = DateTime.Now;
                dbCustomer.UpdateBy = Helper.GetCurrentUserName();

                _customeRepository.Update(dbCustomer);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(dbCustomer);
        }

        //
        // GET: /Customer/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var customer = _customeRepository.GetByID(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        //
        // POST: /Customer/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var customer = _customeRepository.GetByID(id);
            customer.IsDeleted = true;
            _customeRepository.Update(customer);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
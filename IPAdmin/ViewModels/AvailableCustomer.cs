﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using IPAdmin.Models;

namespace IPAdmin.ViewModels
{
    public class AvailableCustomer
    {
        public SerialNo SerialNo { get; set; }
        public ICollection<UserRole> Customers { get; set; }
        public int SelectedCustomerId { get; set; }
        public int SerialNoId { get; set; }

        [Required, MaxLength(100)]
        public string ContractNo { get; set; }

        [Required, MaxLength(100)]
        public string InvoiceNo { get; set; }

        [Required]
        public string Currency { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Display(Name = "Machine Id")]
        public string MachineId { get; set; }
    }
}
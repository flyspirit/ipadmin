﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IPAdmin.Models;

namespace IPAdmin.ViewModels
{
    public class AssignSerialNos
    {
        public IList<SerialNo> SerialNo { get; set; }

        public ICollection<UserRole> Customers { get; set; }

        [Required(ErrorMessage = "Please select a customer")]
        public int SelectedCustomerId { get; set; }

        [Required, MaxLength(100)]
        public string ContractNo { get; set; }

        [Required, MaxLength(100)]
        public string InvoiceNo { get; set; }

        [Required]
        public string Currency { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Display(Name = "Machine Id")]
        public string MachineId { get; set; }
    }
}
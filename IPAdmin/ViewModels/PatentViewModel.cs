﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using IPAdmin.Models;

namespace IPAdmin.ViewModels
{
    public class PatentViewModel
    {
        public PatentViewModel()
        {
            AvailablePatentCountries = new List<PatentCountry>();
        }

        public PatentViewModel(Patent patent)
        {
            IntSearchCode = patent.IntSearchCode;
            Name = patent.Name;
            Owner = patent.Owner;
            Agency = patent.Agency;

            CreateDate = patent.CreateDate;
            CreateBy = patent.CreateBy;
            SerialNoes = patent.SerialNoes;

            AvailablePatentCountries = new List<PatentCountry>();
        }

        public int Id { get; set; }

        [MaxLength(256)]
        [DisplayName("International Publication Number")]
        [Required]
        public string IntSearchCode { get; set; }

        [MaxLength(256)]
        [Required]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Owner { get; set; }

        [MaxLength(256)]
        public string Agency { get; set; }

        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public ICollection<SerialNo> SerialNoes { get; set; }

        [Display(Name = "Country")]
        public ICollection<PatentCountry> AvailablePatentCountries { get; set; }
    }
}
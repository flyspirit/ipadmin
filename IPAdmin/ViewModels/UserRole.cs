﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using IPAdmin.Models;

namespace IPAdmin.ViewModels
{
    public class UserRole
    {
        public UserRole()
        {
        }

        public UserRole(UserProfile user)
        {
            UserId = user.Id;
            UserAndRole = (user.Company ?? user.UserName) + " ---- " + String.Join(",", Roles.GetRolesForUser(user.UserName));
        }

        public int UserId { get; set; }
        public string UserAndRole { get; set; }
    }
}
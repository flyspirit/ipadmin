﻿using IPAdmin.Models;

namespace IPAdmin.ViewModels
{
    public class SerialNoSelection
    {
        public SerialNo SerialNo { get; set; }
        public bool Selected { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using IPAdmin.Models;

namespace IPAdmin.Repository
{
    public class UserProfileRepository : GenericRepository<UserProfile>
    {
        public UserProfileRepository(PatentContext context) : base(context)
        {
        }

        public override UserProfile GetByID(object id)
        {
            var user =  base.GetByID(id);

            if (user.IsDeleted)
                return null;

            return user;
        }

        public virtual IEnumerable<UserProfile> GetAllActive(string includeProperties = "")
        {
            IQueryable<UserProfile> query = dbSet;

            query = query.Where( c => c.IsDeleted == false);

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.ToList();
        }
    }
}
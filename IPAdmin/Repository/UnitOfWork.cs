﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using IPAdmin.Models;

namespace IPAdmin.Repository
{
    public class UnitOfWork
    {
        private PatentContext _context { get; set; }

        public UnitOfWork(PatentContext context)
        {
            _context = context;    
        }

        public GenericRepository<Patent> PatentRepository { get; set; }

        public UserProfileRepository UserProfileRepository { get; set; }

        public GenericRepository<SerialNo> SerialNoRepository { get; set; }

        public GenericRepository<SerialNoCustomer> SerialNoCustomerRepository { get; set; }

        public GenericRepository<Country> CountryRepository { get; set; }

        public void Save()
        {
            try
            {
                _context.SaveChanges();

            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
﻿using System.Data.Entity;
using IPAdmin.Models;

namespace IPAdmin.Repository
{
    public class PatentContext : DbContext
    {
        public PatentContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Patent> Patents { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<SerialNo> SerialNoes { get; set; }
        public DbSet<SerialNoCustomer> SerialNoCustomers { get; set; }
        public DbSet<PatentCountry> PatentCountries { get; set; }
        public DbSet<Country> Countries { get; set; }
    }
}
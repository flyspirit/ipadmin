﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using IPAdmin.ViewModels;

namespace IPAdmin.Models
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(256)]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [MaxLength(256)]
        public string Email { get; set; }

        [MaxLength(256), Required]
        public string Company { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        public DateTime CreateDate { get; set; }

        [MaxLength(256)]
        public string CreateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        [MaxLength(256)]
        public string UpdateBy { get; set; }

        public bool IsDeleted { get; set; }

        public int? ParentUser_Id { get; set; }

        [DisplayName("Developed By")]
        [ForeignKey("ParentUser_Id")]
        public UserProfile ParentUser { get; set; }

        public ICollection<SerialNoCustomer> SerialNoCustomers { get; set; }
        public ICollection<UserProfile> Customers { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Email address is too long")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [MaxLength(255, ErrorMessage = "Company name is too long")]
        [Display(Name = "Company Name")]
        public string Company { get; set; }

        [MaxLength(255, ErrorMessage = "Address is too long")]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name="Type")]
        public string Role { get; set; }

        [Display(Name = "Developed By")]
        public int ParentUserId { get; set; }

        [Display(Name = "Customer of User")]
        public IList<UserRole> AvailableUsers { get; set; }

    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class ResetPasswordModel
    {
        public string UserName { get; set; }
    }

    public class ResetPasswordConfirmModel
    {
        public string Token { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}

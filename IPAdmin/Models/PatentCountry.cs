﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IPAdmin.Models
{
    public class PatentCountry
    {
        [Key]
        public int Id { get; set; }
        public Patent Patent { get; set; }
        public Country Country { get; set; }

        [MaxLength(50)]
        [DisplayName("Application Number")]
        public string SearchCode { get; set; }

        [MaxLength(50)]
        [DisplayName("Patent Number")]
        public string PatentNumber { get; set; }

        [MaxLength(50)]
        public string ResourceCode { get; set; }
    }
}
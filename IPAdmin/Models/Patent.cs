﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IPAdmin.Models
{
    public class Patent
    {
        public Patent()
        {
            PatentCountries = new List<PatentCountry>();
            SerialNoes = new List<SerialNo>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(256)]
        [DisplayName("International Publication Number")]
        public string IntSearchCode { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Owner { get; set; }

        [MaxLength(256)]
        public string Agency { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreateDate { get; set; }
        [MaxLength(50)]
        public string CreateBy { get; set; }
        public ICollection<SerialNo> SerialNoes { get; set; }
        public ICollection<PatentCountry> PatentCountries { get; set; }
    }
}
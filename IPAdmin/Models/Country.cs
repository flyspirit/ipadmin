﻿
using System.ComponentModel.DataAnnotations;

namespace IPAdmin.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
    }
}